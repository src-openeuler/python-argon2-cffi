%global _empty_manifest_terminate_build 0
Name:		python-argon2-cffi
Version:	23.1.0
Release:	1
Summary:	The secure Argon2 password hashing algorithm.
License:	MIT
URL:		https://argon2-cffi.readthedocs.io/
Source0:	%{pypi_source argon2_cffi}
BuildArch:	noarch

%description
CFFI-based Argon2 Bindings for Python

%package -n python3-argon2-cffi
Summary:	The secure Argon2 password hashing algorithm.
Provides:	python-argon2-cffi
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-pip
BuildRequires:	python3-wheel
BuildRequires:	python3-flit
BuildRequires:	python3-hatchling python3-hatch-vcs python3-hatch-fancy-pypi-readme
%description -n python3-argon2-cffi
CFFI-based Argon2 Bindings for Python

%package help
Summary:	Development documents and examples for argon2-cffi
Provides:	python3-argon2-cffi-doc
%description help
CFFI-based Argon2 Bindings for Python

%prep
%autosetup -n argon2_cffi-%{version}

%build
%pyproject_build

%install
%pyproject_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi


%files -n python3-argon2-cffi
%{python3_sitelib}/*
%doc README.md
%license LICENSE

%files help
%{_docdir}/*

%changelog
* Fri Apr 19 2024 yueyuankun <yueyuankun@kylinos.cn> - 23.1.0-1
- Update to 23.1.0
- The InvalidHash exception is deprecated in favor of InvalidHashError
- argon2.hash_password(), argon2.hash_password_raw(), and argon2.verify_password() that have been soft-deprecated since 2016 are now hard-deprecated
- Official support for Python 3.11 and 3.12
- Python 3.6 is not supported anymore
- argon2.exceptions.InvalidHashError as a replacement for InvalidHash
- salt parameter to argon2.PasswordHasher.hash() to allow for custom salts

* Thu Apr 27 2023 wangkai <13474090681@163.com> - 21.3.0-1
- Update to 21.3.0
- Obsoletes subpackage python-argon2-cffi-help

* Wed Aug 04 2021 chenyanpanHW <chenyanpan@huawei.com> - 20.1.0-2
- DESC: delete BuildRequires gdb

* Mon Jul 06 2020 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
